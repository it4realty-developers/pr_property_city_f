<?php
/**
 * @file
 * pr_property_city_f.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function pr_property_city_f_taxonomy_default_vocabularies() {
  return array(
    'property_highway' => array(
      'name' => 'Direction / highway',
      'machine_name' => 'property_highway',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'property_metro' => array(
      'name' => 'Branch / Metro',
      'machine_name' => 'property_metro',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
