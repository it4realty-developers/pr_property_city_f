(function(){
  //для продажи
  searchForm('sale');
  // для аренды
  searchForm('rent');
})();

function searchForm(type){
  var formId = '#views-exposed-form-property-city-property-'+type+'-city',
      $form = jQuery('#views-exposed-form-property-city-property-'+type+'-city'),
      $view = jQuery('div.view-display-id-property_'+type+'_city'); 
      
  //подключаем плагин для зависимостей полей на форме поиск от типа недвижимости
  $form.find('select#edit_type').dependencySearch({
    hideSelector: '.form-element',
    formId: formId
  });
  
  //подключаем плагин для подсчета кол-ва найденных
  $form.find('div#psf-views-result-count-value').resultSearch({
    viewsName: 'property_city',
    displayId: 'property_'+type+'_city',
    formId: formId
  });
  
  //подключим маску для чисел
  $form.find('.pr_search_form--price_sale :text').maskMoney({
    'thousands': " ",
    'precision': "",
    'decimal': ""
  });
  
  $form.find('.form-item-sort-by').clone().appendTo($view.find('div#pr_views_sort'));
  $form.find('.form-item-sort-order').clone().appendTo($view.find('div#pr_views_sort'));
  jQuery("#pr_views_sort .form-item-sort-by select").attr('id', 'pr_views_sort--by');
  jQuery("#pr_views_sort .form-item-sort-order select").attr('id', 'pr_views_sort--order');
  
  $view.find("#pr_views_sort--by").selectmenu({
	width: 120,
    //отслеживаем событие изменения
    change: function() {
      $form.find("#edit-sort-by").val(this.value);
      $form.submit();
    }
  });
  $view.find("#pr_views_sort--order").selectmenu({
	width: 120,
    //отслеживаем событие изменения
    change: function() {
      $form.find("#edit-sort-by").val(this.value);
      $form.submit();
    }
  });
}