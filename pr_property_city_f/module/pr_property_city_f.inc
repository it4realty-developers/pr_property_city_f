<?php

define('CITY_VIEWS_NAME', 'property_city');
define('CITY_DISPLAY_SALE_ID', 'property_sale_city');
define('CITY_DISPLAY_RENT_ID', 'property_rent_city');
define('CITY_FORM_SALE_ID', 'views-exposed-form-property-city-property-sale-city');
define('CITY_FORM_RENT_ID', 'views-exposed-form-property-city-property-rent-city');
define('CITY_VIEWS_SALE_PATH', 'city-sale');
define('CITY_VIEWS_RENT_PATH', 'city-rent');

/**
 * Реализация hook_block_info
 */
function pr_property_city_f_block_info() {
  
  //Блок c картой
  $blocks['pr_property-search_city'] = array(
    'info' => t('Tabs Search').(' (property_city)'), 
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
  
}  

/**
 * Реализация hook_block_view()
 */
function pr_property_city_f_block_view($delta = '') {
  
  $block = array();
    
  switch ($delta) {
    case 'pr_property-search_city':
      $block['content'] = pr_property_city_f_tabs_search_block();
    break;
  }
  
  return $block;
}

/**
 * Блок вкладок для поисковой формы
 */
function pr_property_city_f_tabs_search_block(){
  $render = array();
  
  $render['search_tabs']['#type'] = 'container';
  $render['search_tabs']['#theme'] = 'pr_blocklist';
  $render['search_tabs']['#show_type'] = 'tabs';
  $render['search_tabs']['#weight'] = 0;
  $render['search_tabs']['#pre_render'][] = 'absadmin_wrapper_col_1';
  
  $render['search_tabs']['tab_sale']['#type'] = 'container';
  $render['search_tabs']['tab_sale']['#title_tab'] = t('Sale');
  $render['search_tabs']['tab_sale']['#weight'] = 0;
  $render['search_tabs']['tab_sale']['search_block'] = module_invoke('views', 'block_view', 'ce50b139bb5e96313bea0d98768663de');
  
  $render['search_tabs']['tab_rent']['#type'] = 'container';
  $render['search_tabs']['tab_rent']['#title_tab'] = t('Rent');
  $render['search_tabs']['tab_rent']['#weight'] = 1;
  $render['search_tabs']['tab_rent']['search_block'] = module_invoke('views', 'block_view', '71dcf1f05ae9e13b8536b6c537eb58f3');
  
  return $render;
}

/**
 * Реализация hook_form_alter
 */
function pr_property_city_f_form_alter(&$form, &$form_state, $form_id) {
  
  // форма поиска
  if( ($form['#id'] == CITY_FORM_SALE_ID) || ($form['#id'] == CITY_FORM_RENT_ID)){
    
    // добавляем скрипты на поисковую форму
    $form['#attached']['libraries_load'][] = array('jquery_multiselect');
    // инициализация скриптов скрытия-показа и подсчета количества найденных маскмани
    // клонирование элементов сортировки в шапку вьювса
    $form['#attached']['js'][] = array(
      'data' => drupal_get_path('module', 'pr_property_city_f').'/js/pr_property_city_f.js',
      'type' => 'file',
      'weight' => 200
    );
    
    // type commercial
    $form['commercial_type']['#attributes']['data-plugin'] = 'multiselect';
    $form['commercial_type']['#attributes']['data-selected-list'] = 3;
    $form['commercial_type']['#attributes']['data-uncheck-all-text'] = t('Unselect all');
    $form['commercial_type']['#attributes']['data-none-selected-text'] = t('it4r_all_select_any');
    $form['commercial_type']['#attributes']['data-selected-text'] = t('# it4r_all_select_selected');
    
    $steps_area = '0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 160, 180, 200, 220, 240, 260, 280, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1200, 1400, 1600, 1800, 2000, 2500, 3000, 4000, 5000';
    //area from
    $form['area_from']['#attributes']['placeholder'] = t('from');
    $form['area_from']['#attributes']['data-steps'] = $steps_area;
    $form['area_from']['#attributes']['data-plugin'] = 'slider';
    // area to
    $form['area_to']['#attributes']['placeholder'] = t('to');
    $form['area_to']['#attributes']['data-steps'] = $steps_area;
    $form['area_to']['#attributes']['data-plugin'] = 'slider';
    
    // area plothouse from
    $form['area_plothouse_from']['#attributes']['placeholder'] = t('to');
    $form['area_plothouse_from']['#attributes']['data-steps'] = $steps_area;
    $form['area_plothouse_from']['#attributes']['data-plugin'] = 'slider';
    // area plothouse to
    $form['area_plothouse_to']['#attributes']['placeholder'] = t('from');
    $form['area_plothouse_to']['#attributes']['data-steps'] = $steps_area;
    $form['area_plothouse_to']['#attributes']['data-plugin'] = 'slider';
    
    // area land from
    $form['area_land_from']['#attributes']['placeholder'] = t('from');
    $form['area_land_from']['#attributes']['data-steps'] = $steps_area;
    $form['area_land_from']['#attributes']['data-plugin'] = 'slider';
    // area land to
    $form['area_land_to']['#attributes']['placeholder'] = t('to');
    $form['area_land_to']['#attributes']['data-steps'] = $steps_area;
    $form['area_land_to']['#attributes']['data-plugin'] = 'slider';
    
    // district from
    $form['district_from']['#attributes']['placeholder'] = t('from');
    $form['district_from']['#attributes']['data-steps'] = '0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 30, 50';
    $form['district_from']['#attributes']['data-plugin'] = 'slider';
    // district to
    $form['district_to']['#attributes']['placeholder'] = t('to');
    $form['district_to']['#attributes']['data-steps'] = $steps_area;
    $form['district_to']['#attributes']['data-plugin'] = 'slider';
    
  }
  
  // добавим новые поля 
  if($form_id == 'property_node_form'){
    // вкладкa местоположение
    $form['regs']['reg_tabs']['tab_location']['field_property_distance_district'] = $form['field_property_distance_district'];
    $form['regs']['reg_tabs']['tab_location']['field_property_distance_district']['#pre_render'][] = 'absadmin_wrapper_col_1';
    unset($form['field_property_distance_district']);
    $form['regs']['reg_tabs']['tab_location']['field_property_metro'] = $form['field_property_metro'];
    $form['regs']['reg_tabs']['tab_location']['field_property_metro']['#pre_render'][] = 'absadmin_wrapper_col_1';
    unset($form['field_property_metro']);
    $form['regs']['reg_tabs']['tab_location']['field_property_highway'] = $form['field_property_highway'];
    $form['regs']['reg_tabs']['tab_location']['field_property_highway']['#pre_render'][] = 'absadmin_wrapper_col_1';
    unset($form['field_property_highway']);
  }
  
}

/**
 * Реализация hook_preprocess_views_exposed_form
 */
function pr_property_city_f_preprocess_views_exposed_form(&$vars) {
  
  $form = &$vars['form'];
  if($form['#id'] == CITY_FORM_SALE_ID){
    pr_property_city_f_render_search_sale_from($vars);
  }
  if($form['#id'] == CITY_FORM_RENT_ID){
    pr_property_city_f_render_search_rent_from($vars);
  }
  
}

/**
 * Реализация hook_views_query_alter
 */
function pr_property_city_f_views_query_alter(&$view, &$query) {
  
  if ($view->human_name == CITY_VIEWS_NAME){
    $get = drupal_get_query_parameters();
    
    $price_from = $price_to = '';
    
    //если сущыествует цена от и цена до
    if ( (isset($get['price-from'])) && (isset($get['price-to'])) ) {
      
      $price_to = prPrice::getBaseByValue('to',$get['price-from']);
      $price_from = prPrice::getBaseByValue('from',$get['price-to']);
      
      // для Продажи
      if($view->current_display == CITY_DISPLAY_SALE_ID){
        //для базовой цены до и для базовой цены от
        if( (!empty($price_to) || $price_to == 0) && (!empty($price_from)) ){
          $query->where[1]['conditions'][0]['value'] = $price_to;
          $query->where[1]['conditions'][1]['value'] = $price_from;
        }
      }
      
      // для Аренды
      if($view->current_display == CITY_DISPLAY_RENT_ID){
        switch ($get['period']) {
          case 'day':
            if( (!empty($price_to) || $price_to == 0) && (!empty($price_from)) ){
              //для базовой цены аренды в сутки до и для базовой цены аренды в сутки от
              $query->where[1]['conditions'][0]['value'] = $price_to;
              $query->where[1]['conditions'][1]['value'] = $price_from;
            }
          break;
          case 'week':
            if( (!empty($price_to) || $price_to == 0) && (!empty($price_from)) ){
              //для базовой цены аренды в неделю до и для базовой цены аренды в неделю от
              $query->where[1]['conditions'][2]['value'] = $price_to;
              $query->where[1]['conditions'][3]['value'] = $price_from;
            }
          break;
          case 'month':
            if( (!empty($price_to) || $price_to == 0) && (!empty($price_from)) ){
              //для базовой цены аренды в месяц до и для базовой цены аренды в месяц от
              $query->where[1]['conditions'][4]['value'] = $price_to;
              $query->where[1]['conditions'][5]['value'] = $price_from;
            }
          break;
        }
      }
      
    }
  }
  
}

/**
 * вызывается в контекстном фильтре поискового вьювса
 * для фильтрации по Типу Недвижимости
 */
function pr_property_city_f_get_metro($get){
  if (isset($get['metro']) && is_array($get['metro'])) {
    $tids = array();
    for($i=0; $i<count($get['metro']); $i++) {
      $tids[] = $get['metro'][$i];
    }
    return implode(',', $tids);
  }else{
    return 'all';
  }
}

/**
 * вызывается в контекстном фильтре поискового вьювса
 * для фильтрации по Шоссе
 */
function pr_property_city_f_get_highway($get){
  if (isset($get['highway']) && is_array($get['highway'])) {
    $tids = array();
    for($i=0; $i<count($get['highway']); $i++) {
      $tids[] = $get['highway'][$i];
    }
    return implode(',', $tids);
  }else{
    return 'all';
  }
}

/**
 * Хук для передачи условий работы карты на странице поиска
 * @param type $get - глобальный массив $_GET
 */
function pr_property_city_f_maps_obj_nid($get){
  $post['get'] = $get;
  $post['view_name'] = CITY_VIEWS_NAME;
  if($get['path'] == CITY_VIEWS_SALE_PATH){
    $post['display_id'] = CITY_DISPLAY_SALE_ID;
    return pr_property_api_get_property_search_views($post);
  }
  if($get['path'] == CITY_VIEWS_RENT_PATH){
    $post['display_id'] = CITY_DISPLAY_RENT_ID;
    return pr_property_api_get_property_search_views($post);
  }
}

/**
 *  Хук для формирований аргументов поиска
 * @param type $args - массив для контекстный фильтров вида
 * @param type $get - суперглобальный массив $_GET
 */
function pr_property_city_f_property_search_args(&$args, $get){
  if ( ($get['view_name'] == CITY_VIEWS_NAME) && 
     ( ($get['display_id'] == CITY_DISPLAY_SALE_ID) || ($get['display_id'] == CITY_DISPLAY_RENT_ID) ) 
  ){
    $args = array(
      pr_property_api_get_search_type_tids($get),
      pr_property_api_get_search_location_tids($get),
      pr_property_city_f_get_metro($get),
      pr_property_city_f_get_highway($get)
    );
  }
  
}

/**
 * добавляем в заголовок вьювса поиска колич найденных, перекл карты, 
 * в подвал добавляем карту
 * @param type $vars
 */
function pr_property_city_f_preprocess_views_view(&$vars) {
  if ( ($vars['name'] == CITY_VIEWS_NAME) && 
     ( ($vars['display_id'] == CITY_DISPLAY_SALE_ID) || ($vars['display_id'] == CITY_DISPLAY_RENT_ID) )
  ){
    
    $vars['classes_array'][] = 'pr_views-row_list';
    $vars['classes_array'][] = 'pr_views-row_margin';
    
    $vars['header_left'][0]['#markup'] =  '<div class="pr_views--header_count">'.t('Found objects').': '.$vars['view']->total_rows.'</div>';
    $vars['header_left'][0]['#weight'] =  0;
    
    $vars['header_left'][1] = prMaps::getSwitcher();
    $vars['header_left'][1]['#weight'] = 1;
    
    $vars['footer_map'][0] = prMaps::getSimpleMap();
    $vars['footer_map'][0]['#weight'] = 0;
    
    $vars['header_right'][0]['#markup'] = '<div id="pr_views_sort" class="pr_views--header_sort"></div>';
    $vars['header_right'][0]['#weight'] = 0;
  }
}

/**
 * Реализация hook_theme()
 */
function pr_property_city_f_theme() {
  return array(
    'views_exposed_form__property_city__property_sale_city' => array(
      'render element' => 'form',
      'base hook' => 'views_exposed_form',
      'path' => drupal_get_path('module', 'pr_property_city_f') . '/templates',
      'template' => 'views-exposed-form--property-city--property-sale-city',
    ),
    'views_exposed_form__property_city__property_rent_city' => array(
      'render element' => 'form',
      'base hook' => 'views_exposed_form',
      'path' => drupal_get_path('module', 'pr_property_city_f') . '/templates',
      'template' => 'views-exposed-form--property-city--property-rent-city',
    ),
  );
}

/**
 * Implements template_preprocess_node().
 */
function pr_property_city_f_preprocess_node(&$vars) {
  if($vars['type'] <> 'property'){ return; }
  
  $node = $vars['node'];
  $views_mode = $vars['view_mode'];
  
  // получаем объект недвижимости
  $property_object = new prPropertyCity($node);
  
  // для фулл ===============================================================================================
  if($views_mode == 'full'){
    
    //шоссе
    if($highway = $property_object->getHighway()){
      $highway['#type'] = 'pr_field';
      $highway['items'] = $highway['items_path'];
      $vars['pr_property']['chars_list']['property_highway'] = $highway;
    }
    
    //метро
    if($metro = $property_object->getMetro()){
      $metro['#type'] = 'pr_field';
      $metro['items'] = $metro['items_path'];
      $vars['pr_property']['chars_list']['property_metro'] = $metro;
    }
    
    //расстояние до центра
    if($distance_district = $property_object->getDistanceDistrict()){
      $distance_district['#type'] = 'pr_field';
      $vars['pr_property']['chars_list']['property_distance_district'] = $distance_district;
    }
  }
  
}

/**
 * Реализация hook_preprocess_block()
 */
function pr_local_settings_f_preprocess_block(&$vars) {
  if(($vars['block']->delta == 'ce50b139bb5e96313bea0d98768663de') ||
     ($vars['block']->delta == '71dcf1f05ae9e13b8536b6c537eb58f3')
  ){
    $vars['classes_array'][] = 'pr_content--block-light';
  }
}