<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function pr_property_city_f_render_search_sale_from(&$vars){
  
  pr_property_city_f_render_search_from($vars);
  // тип недвижимости 
  $vars['search_form']['widgets']['type'] = prPropertyBaseWidget::getType('sale', 0, CITY_FORM_SALE_ID, 'edit_type');
  // цена 
  $vars['search_form']['widgets']['price'] = prPropertyBaseWidget::getPrice(30);
}

function pr_property_city_f_render_search_rent_from(&$vars){
  pr_property_city_f_render_search_from($vars);
  // тип недвижимости 
  $vars['search_form']['widgets']['type'] = prPropertyBaseWidget::getType('rent', 0, CITY_FORM_RENT_ID, 'edit_type');
  // цена 
  $vars['search_form']['widgets']['price'] = prPropertyBaseWidget::getPrice(30, 'rent');
}


function pr_property_city_f_render_search_from(&$vars){
  $vars['search_form']['#type'] = 'container';
  $vars['search_form']['#attributes']['id'] = 'pr_search_form';
  $vars['search_form']['#attributes']['class'][] = 'pr_search_form';
  $vars['search_form']['#attributes']['class'][] = 'pr_all_block_ligth';
  
  $vars['search_form']['widgets']['#type'] = 'container';
  $vars['search_form']['widgets']['#attributes']['class'][] = 'row';
  
  //основные виджеты формы =====================================================================================
  // тип коммерческой 
  $vars['search_form']['widgets']['commercial'] = prPropertyBaseWidget::getCommercial($vars['widgets'], 10);
  // местоположение 
  $vars['search_form']['widgets']['location'] = prPropertyBaseWidget::getLocation(20);
  // комнаты 
  $vars['search_form']['widgets']['rooms'] = prPropertyBaseWidget::getBathRooms($vars['widgets'], 40);
  // площадь 
  $vars['search_form']['widgets']['area'] = prPropertyBaseWidget::getArea($vars['widgets'], 50);
  // площадь дома, м2 
  $vars['search_form']['widgets']['area_plothouse'] = prPropertyBaseWidget::getAreaLand($vars['widgets'], 60);
  // площадь участка, сот 
  $vars['search_form']['widgets']['area_land'] = prPropertyBaseWidget::getAreaPlotHouse($vars['widgets'], 70);
  // кнопки 
  $vars['search_form']['widgets']['search_button'] = prPropertyBaseWidget::getButton($vars, 90);
  // кол-во найденных 
  $vars['search_form']['widgets']['search_count'] = prPropertyBaseWidget::getSearchCount(80);
  // сортировка 
  $vars['search_form']['widgets']['search_sort'] = prPropertyBaseWidget::getSort($vars, 100);
  
  //доп виджеты формы =====================================================================================
  // метро 
  $vars['search_form']['widgets']['metro'] = prPropertyCityWidget::getMetro(21);
  // шоссе 
  $vars['search_form']['widgets']['highway'] = prPropertyCityWidget::getHighway(22);
  // расстояние до центра 
  $vars['search_form']['widgets']['district'] = prPropertyCityWidget::getDistanceDistrict($vars['widgets'], 41);
}