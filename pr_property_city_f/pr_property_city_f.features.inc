<?php
/**
 * @file
 * pr_property_city_f.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pr_property_city_f_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
