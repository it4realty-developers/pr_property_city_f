<?php

class prPropertyCity extends prNodeBaseRender{
  
  //расстояние до центра
  public function getDistanceDistrict() {
    if($items = $this->getNodeFieldUnd('field_property_distance_district')){
      return array(
        '#field_name' => 'property_distance_district',
        '#title' => t('Distance to district'),
        '#data_type' => 'intdec',
        '#item_suffix' => ' '.t('km'),
        'items' => $items,
      );
    }else{
      return FALSE;
    }
  }
  
  //шоссе
  public function getHighway() {
    if($items = $this->getNodeFieldUnd('field_property_highway')){
      // добавляем данные для унификации
      foreach ($items as $key => $value) {
        $items[$key]['title'] = prTaxonomy::getTermNameByTid($value['tid'], $this->langcode);
      }
      return array(
        '#field_name' => 'property_highway',
        '#title' => t('Нighway'),
        '#data_type' => 'termlink',
        'items' => $items,
        'items_path' => $this->getTermItemPath($items[0]),
      );
    }else{
      return FALSE;
    }
  }
  
  //метро
  public function getMetro() {
    if($items = $this->getNodeFieldUnd('field_property_metro')){
      // добавляем данные для унификации
      foreach ($items as $key => $value) {
        $items[$key]['title'] = prTaxonomy::getTermNameByTid($value['tid'], $this->langcode);
      }
      return array(
        '#field_name' => 'property_metro',
        '#title' => t('Metro'),
        '#data_type' => 'termlink',
        'items' => $items,
        'items_path' => $this->getTermItemPath($items[0]),
      );
    }else{
      return FALSE;
    }
  }
  
  /**
   * получаем массив-путь из терминов
   * @param type $term
   * @return type
   */
  private function getTermItemPath($term) {
    // строим массив как путь по таксономии
    $i = 0;
    $items_path = array();
    if(isset($term['parents']) && is_array($term['parents'])){
      foreach ($term['parents'] as $tid => $parent_term) {
        $items_path[$i]['tid'] = $parent_term->tid;
        $items_path[$i]['title'] = prTaxonomy::getTermNameByTid($parent_term->tid, $this->langcode);
        $i++;
      }
    }
    $items_path[$i]['tid'] = $term['tid'];
    $items_path[$i]['title'] = prTaxonomy::getTermNameByTid($term['tid'], $this->langcode);
    return $items_path;
  }
  
}
