<?php

class prPropertyCityWidget extends prPropertyBaseWidget{
  
  // свои виджеты =====================================================================================
  // метро
  static public function getMetro($weight, $classes = array()){
    $taxonomy_metro = new prTaxonomy('property_metro');
    $term_metro = $taxonomy_metro->getTree();
    $options_metro = $options_metro_attributes= array();
    if(is_array($term_metro)){
      foreach ($term_metro as $term) {
        $options_metro[$term->tid] = $term->name;
        $options_metro_attributes[$term->tid] = array(
          'class' => 'level'.($term->depth + 1)
        );
      }
    }
    $render = array();
    if(empty($classes)){
      $classes[] = 'col-sm-24';
      $classes[] = 'pr_search_form--metro';
    }
    $render['#type'] = 'container';
    $render['#attributes']['class'] = $classes;
    $render['#weight'] = $weight;
    $render['widget']['#title'] = t('Metro');
    $render['widget']['#type'] = 'select';
    $render['widget']['#options'] = $options_metro;
    $render['widget']['#options_attributes'] = $options_metro_attributes;
    $render['widget']['#value'] = ((isset($_GET['metro'])) ? $_GET['metro'] : '');
    $render['widget']['#attributes']['multiple'] = 'multiple';
    $render['widget']['#attributes']['name'] = 'metro[]';
    $render['widget']['#attributes']['data-plugin'] = 'multiselect';
    $render['widget']['#attributes']['data-selected-list'] = 3;
    $render['widget']['#attributes']['data-uncheck-all-text'] = t('Unselect all');
    $render['widget']['#attributes']['data-none-selected-text'] = t('it4r_all_select_any');
    $render['widget']['#attributes']['data-selected-text'] = t('# it4r_all_select_selected');
    
    return $render;
  }
  // шоссе
  static public function getHighway($weight, $classes = array()){
    $taxonomy_highway = new prTaxonomy('property_highway');
    $term_highway = $taxonomy_highway->getTree();
    $options_highway = $options_highway_attributes = array();
    if(is_array($term_highway)){
      foreach ($term_highway as $term) {
        $options_highway[$term->tid] = $term->name;
        $options_highway_attributes[$term->tid] = array(
          'class' => 'level'.($term->depth + 1)
        );
      }
    }
    $render = array();
    if(empty($classes)){
      $classes[] = 'col-sm-24';
      $classes[] = 'pr_search_form--highway';
    }
    $render['#type'] = 'container';
    $render['#attributes']['class'] = $classes;
    $render['#weight'] = $weight;
    $render['widget']['#title'] = t('Highway');
    $render['widget']['#type'] = 'select';
    $render['widget']['#options'] = $options_highway;
    $render['widget']['#options_attributes'] = $options_highway_attributes;
    $render['widget']['#value'] = ((isset($_GET['highway'])) ? $_GET['highway'] : '');
    $render['widget']['#attributes']['multiple'] = 'multiple';
    $render['widget']['#attributes']['name'] = 'highway[]';
    $render['widget']['#attributes']['data-plugin'] = 'multiselect';
    $render['widget']['#attributes']['data-selected-list'] = 3;
    $render['widget']['#attributes']['data-uncheck-all-text'] = t('Unselect all');
    $render['widget']['#attributes']['data-none-selected-text'] = t('it4r_all_select_any');
    $render['widget']['#attributes']['data-selected-text'] = t('# it4r_all_select_selected');
    
    return $render;
  }
  
  // обертки для виджетов views =====================================================================================
  static public function getDistanceDistrict(&$widgets, $weight, $classes = array()){
    if(empty($classes)){
      $classes[] = 'col-sm-24';
      $classes[] = 'pr_search_form--district';
    }
    return parent::_getWidgetWrapperFromTo($widgets, $weight, $classes, 'filter-field_property_distance_district_value');
  }
  
}
